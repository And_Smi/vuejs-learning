new Vue({
        el: '#exercise',
        data: {
            value: 0
        },
        computed: {
            result: function() {
                if(this.value !== 37){
                    return 'not there yet';
                }
                else{
                    return 'Done!';
                }
            },
        },
        watch: {
            value: function(value) {
                const vm = this;
                setTimeout(function() {
                    return vm.value = 0;
                },5000);
            },
        },
    });